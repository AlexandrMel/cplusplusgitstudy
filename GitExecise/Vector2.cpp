#pragma once


//
// ���� ��������� ������ ������� � ��������� Vector2
//

/// <summary>
/// ��������� �������� ����� ����� ���������.
/// </summary>
/// <param name="a">������ ������ � �����</param>
/// <param name="b">������ ������ � �����</param>
/// <returns>���������� ������ C ��� ��������� �����</returns>
inline  Vector2 Vec2Sums(Vector2 A, Vector2 B)
{
	Vector2 C{ A.x + B.x,A.y + B.y };
	return C;
}